**Prueba de acceso**

Una empresa textil que cuenta con dos almacenes en EEUU desea gestionar de manera eficiente los envíos de su producto a los clientes. Para ello, se deberá desarrollar un software que informe a los trabajadores de dicha empresa acerca de cómo gestionar cada pedido de la manera más eficiente posible. En primer lugar, se deberá elegir la caja más pequeña posible en la que se enviará el producto teniendo en cuenta el máximo peso que es capaz de soportar y que el producto tiene que caber en la caja. Por otro lado se tendrá que elegir el almacén de envío teniendo en cuenta el precio y hora del envío. El precio depende del volumen de la caja y del precio de experiencia y la hora de salida del almacén tiene que coincidir con las horas de recogida de los transportistas. En caso de que el precio del envío sea igual desde ambos almacenes se elegirá aquel que tenga más existencias del producto. Finalmente deberá quedar reflejado el coste total de todos los envíos.

**Pasos a seguir**


1. Implementar la función `findBestBoxType`: Encuentra la caja más pequeña en la que quepa el producto a enviar.
2. Implementar la funcion `findBestRoute`: Elige el almacen desde donde se va a enviar el producto en función del coste y la hora. Para mayor facilidad se ha implementado la funcion `getDeliveryDateTime` que calcula la hora de entrega del producto desde el almacen. 

El programa obtiene los datos desde `input.txt` y genera un `output.txt` donde se encuentran los diferentes envios que se van a realizar con su correspondiente coste. El primer valor que aparece en el fichero `output.txt` es el coste total de todos los envíos.
En el proyecto aparece ya generado el fichero `output.txt` resultante a partir del `input.txt` dado.

______________________________________________________________________________________________________________________________________________

Solución Alicia Gálvez

* Para la funcion `findBestBoxType` he tenido en cuenta que el item se puede girar, por lo que las dimensiones no tienen por qué coincidir (alto con alto, etc). Para ello, he utilizado una lista, ordenándola de menor a mayor según sus dimensiones, para así compararlas con los dimensiones ordenadas de las cajas. He utilizado un algoritmo voraz ya que al estar todo ordenado, se garantiza que vamos a cojer el mejor. 

* En la función `findBestRoute` hay aspectos que se podrían hacer más eficientes, como por ejemplo no llamar dos veces al método `getShippingPrice` y realizar un bucle que obtenga directamente el mejor precio. Sin embargo, perdería el peor precio, y en caso de querer cambiar de almacen más adelante no tendría dicha variable. 

No tenía claro las prioridades, por ello he definido que, cuando en ambos almacenes tienen el mismo 'ShippingPrice' y mismo stock, se tenga en cuenta el almacen que menos tiempo tarde en hacer llegar el paquete. Finalmente he obtenido un coste infenior al propuesto en el output inicial, por lo que intuyo puede ser una buena aproximación. 

Por último, quería añadir que mejoraría la eficiencia general del programa utilizando mapas o biMapas en lugar de listas, ya que simplifican la complejidad de busqueda a O(1). Por ejemplo, utilizar un mapa en lugar de una lista de Items, siendo la clave el itemId (es único) y como valor el objeto Item. Sin embargo, no he querido realizar dichos cambios para adaptarme al código ya existente e implementar únicamente las funciones propuestas. 

